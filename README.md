Rapport TP exploitation des données.

Ce rapport explique le processus d'extraction, de nettoyage et de visualisation des données du site des JO. Les données extraites comprennent des informations sur les pays participants, les sports et les athlètes.
Extraction des Données
Trois scripts de scraping ont été développés pour extraire les données des différentes catégories : pays, sports et athlètes.
scrape_athlete.py : Ce script extrait les données sur les athlètes olympiques, y compris le nombre de médailles d'or, d'argent et de bronze remportées par chaque athlète.  Pour se faire, on commence par accéder au site. Les athlètes sont triés par ordre alphabétique de la première lettre du nom de famille, donc on parcourt chaque page une par une pour obtenir les athlètes. On récupère ensuite chaque athlète, on accède à son profil et on récupère les médailles qu’il a gagnées.
scrape_countries.py : Ce script extrait les données sur les pays participants, y compris le nombre de médailles d'or, d'argent et de bronze remportées. Pour les médailles des pays, on procède presque comme pour les athlètes, sauf que ceux-ci ne sont pas triés par ordre alphabétique, donc l’accès y est plus simple, pareil pour les sports.
scrape_sport.py : Ce script récupère les informations sur les sports olympiques, ainsi que les médailles remportées par chaque pays dans chaque sport. 
Nettoyage et Transformation des Données
Un script de nettoyage des données (clean_data.py) a été développé pour nettoyer les données extraites.
Suppression des caractères indésirables, des espaces blancs superflus et des balises HTML.
Correction des erreurs de typographie et de formatage.
Standardisation des noms de pays, des noms de disciplines sportives et des noms d'athlètes.
Données Nettoyées
Les données nettoyées sont stockées dans des fichiers JSON dans le répertoire ./data/clean/.
countries_data.json : Contient les données nettoyées sur les médailles par pays.
sports_data.json : Contient les données nettoyées sur les médailles par sport.
athletes_data.json : Contient les données nettoyées sur les médailles par athlète.
Visualisation des Données
Les données nettoyées sont visualisées à l'aide de graphiques en camembert (pie charts) créés avec Highcharts.
Médailles par Pays : Ce graphique montre la répartition des médailles d'or, d'argent et de bronze par pays.
Médailles par Sport : Ce graphique présente la répartition des médailles d'or, d'argent et de bronze par sport et par pays.
Médailles par Athlète : Ce graphique illustre la répartition des médailles d'or, d'argent et de bronze par athlète.
Répertoire Git et Intégration
Le code source, les fichiers de données, le rapport et la page HTML avec les graphiques sont intégrés dans un dépôt Git hébergé sur GitHub. Pour afficher la page HTML, on utilisera http://localhost:8000/.