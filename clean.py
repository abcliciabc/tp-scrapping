import json
import re
from bs4 import BeautifulSoup

# Fonction pour nettoyer une chaîne de caractères en supprimant les balises HTML et les espaces blancs superflus
def clean_string(s):
    if not s:
        return ""
    # Supprimer les balises HTML
    s = BeautifulSoup(s, "html.parser").get_text()
    # Supprimer les espaces blancs superflus et les caractères indésirables
    return re.sub(r'\s+', ' ', s).strip()

# Fonction pour standardiser les noms (par exemple, mettre en majuscule la première lettre de chaque mot)
def standardize_name(name):
    return ' '.join(word.capitalize() for word in name.split())

def clean_country_data(country):
    country['country'] = standardize_name(clean_string(country['country']))
    country['medals'] = {
        'gold': int(country['medals']['gold']),
        'silver': int(country['medals']['silver']),
        'bronze': int(country['medals']['bronze'])
    }
    return country

def clean_sport_data(sport):
    sport['sport'] = standardize_name(clean_string(sport['sport']))
    for nation in sport['nations']:
        nation['country'] = standardize_name(clean_string(nation['country']))
        nation['medals'] = {
            'gold': int(nation['medals']['gold']),
            'silver': int(nation['medals']['silver']),
            'bronze': int(nation['medals']['bronze'])
        }
    return sport

def clean_athlete_data(athlete):
    athlete['athlete'] = standardize_name(clean_string(athlete['athlete']))
    if athlete['medals']:
        athlete['medals'] = {
            'gold': int(athlete['medals']['gold']),
            'silver': int(athlete['medals']['silver']),
            'bronze': int(athlete['medals']['bronze'])
        }
    return athlete

def load_and_clean_data(filepath, cleaner):
    with open(filepath, 'r') as f:
        data = json.load(f)
    cleaned_data = [cleaner(item) for item in data]
    return cleaned_data

def save_data(filepath, data):
    with open(filepath, 'w') as f:
        json.dump(data, f, indent=4)

if __name__ == '__main__':
    countries = load_and_clean_data('./data/raw/countries_data.json', clean_country_data)
    save_data('./data/clean/countries_data.json', countries)

    sports = load_and_clean_data('./data/raw/sports_data.json', clean_sport_data)
    save_data('./data/clean/sports_data.json', sports)

    athletes = load_and_clean_data('./data/raw/athletes_data.json', clean_athlete_data)
    save_data('./data/clean/athletes_data.json', athletes)

    print("Data cleaned and saved successfully.")
