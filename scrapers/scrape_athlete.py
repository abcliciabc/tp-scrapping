import requests
from bs4 import BeautifulSoup
import json
import asyncio

base_url = "https://olympics-statistics.com"

async def get_soup(url):
    response = await loop.run_in_executor(None, requests.get, url)
    return BeautifulSoup(response.text, 'html.parser')

async def scrape_athlete_medals(relative_url):
    soup = await get_soup(base_url + relative_url)
    if soup is None:
        return []
    medals = {'gold': 0, 'silver': 0, 'bronze': 0}
    medal_data_div = soup.find('div', class_='rnd teaser')
    if medal_data_div is not None:
        for medal_div in medal_data_div:
            if 'the-medal' in str(medal_div):
                print('MEDAL_DIV :', medal_div)
                medal_count = int(medal_div.find('span', class_='mal').text.strip())
                medal_type = int(medal_div.find('div', class_='the-medal')['data-medal'])
                if medal_type == 1:
                    medals['gold'] = medal_count
                elif medal_type == 2:
                    medals['silver'] = medal_count
                elif medal_type == 3:
                    medals['bronze'] = medal_count
        print(medals)
        return medals

async def scrape_athletes_by_letter(letter):
    url = f"{base_url}/olympic-athletes/{letter}"
    soup = await get_soup(url)
    if soup is None:
        return []
    athlete_links = soup.find_all('a', class_='card athlet visible')
    athlete_urls = [link.get('href') for link in athlete_links]
    print(athlete_urls)
    return athlete_urls

async def scrape_all_athletes_medals():
    all_medals = []
    tasks = []
    for letter in ('e'): # pour l'alphabet complet : string.ascii_lowercase
        athletes_urls = await scrape_athletes_by_letter(letter)
        for athlete_url in athletes_urls:
            athlete_name = athlete_url.split('/')[-2]
            task = asyncio.ensure_future(scrape_athlete_medals(athlete_url))
            tasks.append(task)
            all_medals.append({
                'athlete': athlete_name,
                'medals': None
            })
    medals_results = await asyncio.gather(*tasks)
    for i, medals in enumerate(medals_results):
        all_medals[i]['medals'] = medals
    print(all_medals)
    return all_medals

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    data = loop.run_until_complete(scrape_all_athletes_medals())
    loop.close()
    with open('./data/raw/athletes_data.json', 'w') as f:
        json.dump(data, f, indent=4)
    print("Data scraped and saved successfully.")
