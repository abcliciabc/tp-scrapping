import requests
from bs4 import BeautifulSoup
import json

base_url = "https://olympics-statistics.com"

session = requests.Session()

def get_soup(url):
    response = session.get(url)
    return BeautifulSoup(response.text, 'html.parser')

def scrape_countries():
    url = f"{base_url}/nations"
    soup = get_soup(url)
    country_cards = soup.find_all('a', class_='card nation visible')
    countries = []
    for card in country_cards:
        country_link = card.get('href')
        countries.append(scrape_country_details(country_link))
    return countries

def scrape_country_details(relative_url):
    url = base_url + relative_url
    soup = get_soup(url)
    medals = {'gold': 0, 'silver': 0, 'bronze': 0}
    country_name = soup.find('div', class_='landname').text.strip()
    medal_data_div = soup.find('div', class_='rnd teaser')
    if medal_data_div is not None:
        for medal_div in medal_data_div:
            if 'the-medal' in str(medal_div):
                print('MEDAL_DIV :', medal_div)
                medal_count = int(medal_div.find('span', class_='mal').text.strip())
                type_medal = int(medal_div.find('div', class_='the-medal')['data-medal'])
                print('type_medal:', type_medal, 'medal_count:', medal_count)
                if type_medal == 1:
                    medals['gold'] = medal_count
                elif type_medal == 2:
                    medals['silver'] = medal_count
                elif type_medal == 3:
                    medals['bronze'] = medal_count
        details = {
            "country": country_name,
            "medals": medals
        }
        print(details)
        return details

if __name__ == '__main__':
    countries_data = scrape_countries()
    with open('./data/raw/countries_data.json', 'w') as f:
        json.dump(countries_data, f, indent=4)
    print("Data scraped and saved successfully.")

