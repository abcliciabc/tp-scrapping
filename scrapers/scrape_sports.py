import requests
from bs4 import BeautifulSoup
import json

base_url = "https://olympics-statistics.com"

def get_soup(url):
    response = requests.get(url)
    if response.status_code == 200:
        return BeautifulSoup(response.text, 'html.parser')
    else:
        print("Failed to retrieve data:", response.status_code)
        return None

def scrape_sports_list():
    url = f"{base_url}/olympic-sports"
    soup = get_soup(url)
    if soup is None:
        return []
    sports_links = soup.find_all('a', class_='card sport visible')
    sports_urls = [link.get('href') for link in sports_links]
    return sports_urls

def scrape_medals_by_nation(sport_url):
    soup = get_soup(base_url + sport_url)
    if soup is None:
        return []
        
    nation_cards = soup.find_all('div', class_='card nation visible')
    nations = []
    for card in nation_cards:
        country_name = card.find('div', class_='n').text.strip()
        medals_divs = card.find_all('div', attrs={'data-c': True})
        medals = {'gold': 0, 'silver': 0, 'bronze': 0}
        for medal_div in medals_divs:
            print(medal_div) # <div data-c="11"><span class="mal">11</span><div class="the-medal" data-medal="2"></div></div>
            medal_count = int(medal_div.find('span', class_='mal').text.strip())
            medal_type = int(medal_div.find('div', class_='the-medal')['data-medal'])
            if medal_type == 1:
                medals['gold'] = medal_count
            elif medal_type == 2:
                medals['silver'] = medal_count
            elif medal_type == 3:
                medals['bronze'] = medal_count
        nations.append({
            'country': country_name,
            'medals': medals
        })
    return nations

def scrape_all_sports_medals():
    sports_urls = scrape_sports_list()
    all_medals = []
    for sport_url in sports_urls:
        sport_name = sport_url.split('/')[-2]
        medals = scrape_medals_by_nation(sport_url)
        all_medals.append({
            'sport': sport_name,
            'nations': medals
        })
    return all_medals

if __name__ == '__main__':
    data = scrape_all_sports_medals()
    with open('./data/raw/sports_data.json', 'w') as f:
        json.dump(data, f, indent=4)
    print("Data scraped and saved successfully.")
